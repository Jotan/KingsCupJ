import { Component,EventEmitter,Output } from '@angular/core';
import { cardService } from '../../service/cardService'
import * as $ from 'jquery'

/* 
  Generated class for the FlashCard component.

  See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
  for more info on Angular 2 Components.
*/
@Component({
  selector: 'flash-card',
  templateUrl: 'flash-card.html',
  providers: [cardService]
})
export class FlashCardComponent {
  flipped: boolean = false;
  cards: number[] = [52];
  n: number = 0;
  cont: number = 0;
  j: number = 0;
  idCard: Number;
  @Output() idText = new EventEmitter();
  constructor(public cardservice: cardService) {
    // this.idCard = cardservice.getIdCard();    
  }



  ngOnInit() {  
    for (let i = 0; i < 52; i++) {
      if (i == 13 || i == 26 || i == 39) {
        this.j = 0;
      }
      this.cards[i] = this.j;
      this.j++;
    }
    console.log(this.cards)
    this.cards = this.shuffle(this.cards);
  }



  shuffle(array) {
    var currentIndex = array.length, temporaryValue, randomIndex;
    // While there remain elements to shuffle...
    while (0 !== currentIndex) {
      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;
      // And swap it with the current element.
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }
    return array;
  }

  changeImage() {
    this.flip()
    this.cardservice.setIdCard(this.idCard)
    this.idText.emit(this.idCard)
}


  flip() {
    var id
    this.cont++;
    if (this.cards.length != 0) {
      this.flipped = !this.flipped;
      if (this.n % 2 == 0) {
        id = this.cards[0];
        this.cardservice.setIdCard(id);        
        this.cards.shift()
        $(function () {
          $('.cardBackground').click(function () {
            var that = this;
            window.setTimeout(function () {
              $(that).css('background-image', 'url(\'https://trex.tk/kc/img/' + id + '.jpg\')');
            }, 180);
          });
        })
      }
      this.idCard = id
      this.n++;
    } else {
      this.flipped = false
      console.log("Game finished")
    }
  }
}
