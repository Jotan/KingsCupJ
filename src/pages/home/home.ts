import { Component, Directive } from '@angular/core';
import { cardService } from '../../service/cardService'
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  //directives: [flash-card],
  providers: [cardService]

})
export class HomePage {
  cS: cardService;
  idCard: Number;
  flashCards: any
  description: String;
  constructor(public navCtrl: NavController, public cardS: cardService) {
    this.flashCards = [
      {}
      // {front: 'Hello', back: 'GoodBye'},
      // {front: 'Konichiwa', back: 'Konbanwa'},
      // {front: 'Sawadi Krap', back: 'Sawadi Ka'}

    ];



  }


  setText(id: Number) {
    var ids = id
    switch (ids) {
      case 0:
        this.description = "WATERFALL!!";
        break;
      case 1:
        this.description = "Two for you!!"
        break;

      case 2:
        this.description = "Three for me, yeah!"
        break;

      case 3:
        this.description = "Neighbours, please!";
        break;

      case 4:
        this.description = "Five? Guys!"
        break;

      case 5:
        this.description = "Six? Chicks!";
        break;

      case 6:
        this.description = "Hands Up!"
        break;

      case 7:
        this.description = "Maid grrr"
        break;

      case 8:
        this.description = "Boom and Oil!"
        break;

      case 9:
        this.description = "Categories!"
        break;

      case 10:
        this.description = "New Rule!";
        break;

      case 11:
        this.description = "QuestionMaster!"
        break;

      case 12:
        this.description = "KING KING KING!!!"
        break;

      default:
        this.description = "Well.. should not appear this! >.<"
    }
  }

}
